package com.example.msaweb.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ViewController {

	@GetMapping("/")
	public String index(Model mode) {
		
		return "index";
	}
}
